$(function(){
          $("[data-toggle='tooltip']").tooltip();
          $("[data-toggle='popover']").popover();
          $('.carousel').carousel({
            interval: 4000
          });

          $('#contacto').on('show.bs.modal', function (e){
            console.log('el modal se esta mostrando');

            $('#contactoBtn').removeClass('btn-outline-success').addClass('btn-primary').prop('disabled', true);

          });
          $('#contacto').on('shown.bs.modal', function (e){
            console.log('el modal se mostro');
          });
          $('#contacto').on('hide.bs.modal', function (e){
            console.log('el modal se esta ocultando');
          });
          $('#contacto').on('hidden.bs.modal', function (e){
            console.log('el modal se oculto');

            $('#contactoBtn').removeClass('btn-primary').addClass('btn-outline-success').prop('disabled', false);

          });
      });